package com.portal.tests;

import com.portal.framework.TestBase;
import com.portal.pageFactory.*;
import com.portal.utils.DBUtil;
import org.junit.Assert;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.*;
import java.io.IOException;

public class LoginTest extends TestBase {

    @BeforeClass
    public void initialize() throws Exception {
        initialization();
    }

    @BeforeTest
    public void clearTestDatA(){
        try {
           dbUtil=new DBUtil(propFileRead().getProperty("db_url"),
                    propFileRead().getProperty("db_uname"),
                    propFileRead().getProperty("db_pwd"));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

   @Test
    public void sanityOKYC() throws Exception {
       LoginPage loginPage = PageFactory.initElements(driver, LoginPage.class);
       loginPage.prepareForTest(propFileRead().getProperty("mobile_number"));
       loginPage.loginApplication(propFileRead().getProperty("mobile_number"));
       Assert.assertTrue(loginPage.checkOTPWindow());
       loginPage.setOTP();
       loginPage.clickVerifyNow();
       CheckEligibilityPage checkEligibilityPage= PageFactory.initElements(driver,CheckEligibilityPage.class);
       checkEligibilityPage.selectProfessionalDetails(propFileRead().getProperty("professionalType"));
       checkEligibilityPage.selectIncomeMode("online");
       checkEligibilityPage.setMonthlyTakeHome("15500");
       checkEligibilityPage.setEmail(propFileRead().getProperty("email"));
       checkEligibilityPage.isSelfEmpNetBankingEnabled("YES");
       Thread.sleep(2000);
       checkEligibilityPage.clickSelectBankICICIBank();
       Thread.sleep(2000);
       checkEligibilityPage.clickSelectPurposeOfLoan();
       checkEligibilityPage.clickContinueButton();
       checkEligibilityPage.selectGender();
       checkEligibilityPage.setFirstName(propFileRead().getProperty("firstName"));
       checkEligibilityPage.setLastName(propFileRead().getProperty("lastName"));
       checkEligibilityPage.setDob(propFileRead().getProperty("dateOfBirth"));
       checkEligibilityPage.setAreaPinCode(propFileRead().getProperty("areaPinCode"));
       checkEligibilityPage.setPan(propFileRead().getProperty("panNumber"));
       checkEligibilityPage.clickAuthorization();
       checkEligibilityPage.clickContinuePersonalDetailsBtn();
       checkEligibilityPage.clickConfirmDetails();
       Thread.sleep(10000);
       GetOfferPage getOfferPage=PageFactory.initElements(driver,GetOfferPage.class);
       getOfferPage.clickUploadBnkStmt();
       getOfferPage.updateBankStmt(this.getClass().getClassLoader().getResource("ICICI.pdf").getFile());
       getOfferPage.clickPasswordProtectedCheckBox();
       getOfferPage.setPdfPassword(propFileRead().getProperty("stmtPassword"));
       getOfferPage.clickVerifyNowBtn();
       getOfferPage.clickConfirm();
       SelectPlanPage selectPlanPage=PageFactory.initElements(driver,SelectPlanPage.class);
       Thread.sleep(45000);
       selectPlanPage.clickContinue();
       selectPlanPage.clickSendOtp();
       Assert.assertEquals(propFileRead().getProperty("email"),selectPlanPage.getNameText());
       selectPlanPage.setOtp();
       selectPlanPage.clickVerifyNow();
       KYCPage kycPage=PageFactory.initElements(driver,KYCPage.class);
       kycPage.clickVerifyNow();
       //test ends here for automation ,Because captcha is appearnig for OKYC


    }


    @Test
    public void sanityManual() throws Exception {
        LoginPage loginPage = PageFactory.initElements(driver, LoginPage.class);
        loginPage.prepareForTest(propFileRead().getProperty("mobile_number"));
        loginPage.loginApplication(propFileRead().getProperty("mobile_number"));
        Assert.assertTrue(loginPage.checkOTPWindow());
        loginPage.setOTP();
        loginPage.clickVerifyNow();
        CheckEligibilityPage checkEligibilityPage= PageFactory.initElements(driver,CheckEligibilityPage.class);
        checkEligibilityPage.selectProfessionalDetails(propFileRead().getProperty("professionalType"));
        checkEligibilityPage.selectIncomeMode("online");
        checkEligibilityPage.setMonthlyTakeHome("60500");
        checkEligibilityPage.setEmail(propFileRead().getProperty("email"));
        checkEligibilityPage.isNetBankingEnabled("YES");
        Thread.sleep(2000);
        checkEligibilityPage.clickSelectBankICICIBank();
        Thread.sleep(2000);
        checkEligibilityPage.clickSelectPurposeOfLoan();
        checkEligibilityPage.clickContinueButton();
        checkEligibilityPage.selectGender();
        checkEligibilityPage.setFirstName(propFileRead().getProperty("firstName"));
        checkEligibilityPage.setLastName(propFileRead().getProperty("lastName"));
        checkEligibilityPage.setDob(propFileRead().getProperty("dateOfBirth"));
        checkEligibilityPage.setAreaPinCode(propFileRead().getProperty("areaPinCode"));
        checkEligibilityPage.setPan(propFileRead().getProperty("panNumber"));
        checkEligibilityPage.clickAuthorization();
        checkEligibilityPage.clickContinuePersonalDetailsBtn();
        checkEligibilityPage.clickConfirmDetails();
        Thread.sleep(6000);
        GetOfferPage getOfferPage=PageFactory.initElements(driver,GetOfferPage.class);
        getOfferPage.clickUploadBnkStmt();
        getOfferPage.updateBankStmt(this.getClass().getClassLoader().getResource("ICICI.pdf").getFile());
        getOfferPage.clickPasswordProtectedCheckBox();
        getOfferPage.setPdfPassword(propFileRead().getProperty("stmtPassword"));
        getOfferPage.clickVerifyNowBtn();
        getOfferPage.clickConfirm();
        SelectPlanPage selectPlanPage=PageFactory.initElements(driver,SelectPlanPage.class);
        Thread.sleep(30000);
        selectPlanPage.clickContinue();
        selectPlanPage.clickSendOtp();
        Assert.assertEquals(propFileRead().getProperty("email"),selectPlanPage.getNameText());
        selectPlanPage.setOtp();
        selectPlanPage.clickVerifyNow();
        KYCPage kycPage=PageFactory.initElements(driver,KYCPage.class);
        kycPage.verifyManually();
        kycPage.clickVerify();
        ExtraInfoPage extraInfoPage= PageFactory.initElements(driver,ExtraInfoPage.class);
        extraInfoPage.clickPhoto();
        extraInfoPage.uploadVideo();
        extraInfoPage.clickPanDiv();
        extraInfoPage.setPanCard(this.getClass().getClassLoader().getResource("PAN-Card.jpg").getFile());
        extraInfoPage.clickUploadPan();
        extraInfoPage.setAddressDetails();
        extraInfoPage.setAccountDetails();
        extraInfoPage.uploadCheque();
        extraInfoPage.location();
        extraInfoPage.setAdditionalDetails();
        extraInfoPage.clickContinueBtn();
        extraInfoPage.clickSubmitApp();
    }


    @Test
    public void softRejectSalaryCriteriaFlow() throws Exception {
        LoginPage loginPage = PageFactory.initElements(driver, LoginPage.class);
        loginPage.prepareForTest(propFileRead().getProperty("mobile_number"));
        loginPage.loginApplication(propFileRead().getProperty("mobile_number"));
        Thread.sleep(5000);
        Assert.assertTrue(loginPage.checkOTPWindow());
        loginPage.setOTP();
        loginPage.clickVerifyNow();
        CheckEligibilityPage checkEligibilityPage= PageFactory.initElements(driver,CheckEligibilityPage.class);
        checkEligibilityPage.selectProfessionalDetails(propFileRead().getProperty("professionalType"));
        checkEligibilityPage.selectIncomeMode("online");
        checkEligibilityPage.setMonthlyTakeHome("12500");
        checkEligibilityPage.setEmail("testAutomation@gmail.com");
        checkEligibilityPage.isNetBankingEnabled("YES");
        Thread.sleep(2000);
        checkEligibilityPage.clickSelectBankICICIBank();
        Thread.sleep(2000);
        checkEligibilityPage.clickSelectPurposeOfLoan();
        checkEligibilityPage.clickContinueButton();
        Assert.assertTrue(checkEligibilityPage.getSalaryEligibilityError());
    }



    @Test
    public void softRejectAgeCriteriaFlow() throws Exception {
        LoginPage loginPage = PageFactory.initElements(driver, LoginPage.class);
        loginPage.prepareForTest(propFileRead().getProperty("mobile_number"));
        loginPage.loginApplication(propFileRead().getProperty("mobile_number"));
        Thread.sleep(2000);
        Assert.assertTrue(loginPage.checkOTPWindow());
        loginPage.setOTP();
        loginPage.clickVerifyNow();
        CheckEligibilityPage checkEligibilityPage= PageFactory.initElements(driver,CheckEligibilityPage.class);
        checkEligibilityPage.selectProfessionalDetails(propFileRead().getProperty("professionalType"));
        checkEligibilityPage.selectIncomeMode("online");
        checkEligibilityPage.setMonthlyTakeHome("60500");
        checkEligibilityPage.setEmail(propFileRead().getProperty("email"));
        checkEligibilityPage.isNetBankingEnabled("YES");
        Thread.sleep(2000);
        checkEligibilityPage.clickSelectBankICICIBank();
        Thread.sleep(2000);
        checkEligibilityPage.clickSelectPurposeOfLoan();
        checkEligibilityPage.clickContinueButton();
        checkEligibilityPage.selectGender();
        checkEligibilityPage.setFirstName(propFileRead().getProperty("firstName"));
        checkEligibilityPage.setLastName(propFileRead().getProperty("lastName"));
        checkEligibilityPage.setDob(propFileRead().getProperty("invalidDateOfBirth"));
        checkEligibilityPage.setAreaPinCode(propFileRead().getProperty("areaPinCode"));
        checkEligibilityPage.setPan(propFileRead().getProperty("panNumber"));
        checkEligibilityPage.clickAuthorization();
        checkEligibilityPage.clickContinuePersonalDetailsBtn();
        Assert.assertTrue(checkEligibilityPage.checkAgeSoftRejectMessage());
    }


    @Test
    public void softRejectServiceablePinCodeCriteriaFlow() throws Exception {
        LoginPage loginPage = PageFactory.initElements(driver, LoginPage.class);
        loginPage.prepareForTest(propFileRead().getProperty("mobile_number"));
        loginPage.loginApplication(propFileRead().getProperty("mobile_number"));
        Thread.sleep(2000);
        Assert.assertTrue(loginPage.checkOTPWindow());
        loginPage.setOTP();
        loginPage.clickVerifyNow();
        CheckEligibilityPage checkEligibilityPage= PageFactory.initElements(driver,CheckEligibilityPage.class);
        checkEligibilityPage.selectProfessionalDetails(propFileRead().getProperty("professionalType"));
        checkEligibilityPage.selectIncomeMode("online");
        checkEligibilityPage.setMonthlyTakeHome("60500");
        checkEligibilityPage.setEmail(propFileRead().getProperty("email"));
        checkEligibilityPage.isNetBankingEnabled("YES");
        Thread.sleep(2000);
        checkEligibilityPage.clickSelectBankICICIBank();
        Thread.sleep(2000);
        checkEligibilityPage.clickSelectPurposeOfLoan();
        checkEligibilityPage.clickContinueButton();
        checkEligibilityPage.selectGender();
        checkEligibilityPage.setFirstName(propFileRead().getProperty("firstName"));
        checkEligibilityPage.setLastName(propFileRead().getProperty("lastName"));
        checkEligibilityPage.setDob(propFileRead().getProperty("dateOfBirth"));
        checkEligibilityPage.setAreaPinCode(propFileRead().getProperty("invalidAreaPinCode"));
        checkEligibilityPage.setPan(propFileRead().getProperty("panNumber"));
        checkEligibilityPage.clickAuthorization();
        checkEligibilityPage.clickContinuePersonalDetailsBtn();
        Assert.assertTrue(checkEligibilityPage.checkServiceableAreaPinCodeSoftRejectMessage());
    }




    @Test
    public void sanityCashModeOfPaymentSoftReject() throws Exception {
        LoginPage loginPage = PageFactory.initElements(driver, LoginPage.class);
        loginPage.prepareForTest(propFileRead().getProperty("mobile_number"));
        loginPage.loginApplication(propFileRead().getProperty("mobile_number"));
        Assert.assertTrue(loginPage.checkOTPWindow());
        loginPage.setOTP();
        loginPage.clickVerifyNow();
        CheckEligibilityPage checkEligibilityPage = PageFactory.initElements(driver, CheckEligibilityPage.class);
        checkEligibilityPage.selectProfessionalDetails(propFileRead().getProperty("professionalType"));
        checkEligibilityPage.selectIncomeMode("cash");
        checkEligibilityPage.setMonthlyTakeHome("15500");
        checkEligibilityPage.setEmail(propFileRead().getProperty("email"));
        checkEligibilityPage.clickContinueButton();
        Assert.assertTrue(checkEligibilityPage.cashSoftRejectPopUp());
    }




    @Test
    public void chequeNetBankingNotEnabled() throws Exception {
        LoginPage loginPage = PageFactory.initElements(driver, LoginPage.class);
        loginPage.prepareForTest(propFileRead().getProperty("mobile_number"));
        loginPage.loginApplication(propFileRead().getProperty("mobile_number"));
        Assert.assertTrue(loginPage.checkOTPWindow());
        loginPage.setOTP();
        loginPage.clickVerifyNow();
        CheckEligibilityPage checkEligibilityPage= PageFactory.initElements(driver,CheckEligibilityPage.class);
        checkEligibilityPage.selectProfessionalDetails(propFileRead().getProperty("professionalType"));
        checkEligibilityPage.selectIncomeMode("cheque");
        checkEligibilityPage.setMonthlyTakeHome("60500");
        checkEligibilityPage.setEmail(propFileRead().getProperty("email"));
        checkEligibilityPage.isNetBankingEnabled("NO");
        Thread.sleep(2000);
        checkEligibilityPage.clickSelectBankICICIBank();
        Thread.sleep(2000);
        checkEligibilityPage.clickSelectPurposeOfLoan();
        checkEligibilityPage.clickContinueButton();
        checkEligibilityPage.selectGender();
        checkEligibilityPage.setFirstName(propFileRead().getProperty("firstName"));
        checkEligibilityPage.setLastName(propFileRead().getProperty("lastName"));
        checkEligibilityPage.setDob(propFileRead().getProperty("dateOfBirth"));
        checkEligibilityPage.setAreaPinCode(propFileRead().getProperty("areaPinCode"));
        checkEligibilityPage.setPan(propFileRead().getProperty("panNumber"));
        checkEligibilityPage.clickAuthorization();
        checkEligibilityPage.clickContinuePersonalDetailsBtn();
        checkEligibilityPage.clickConfirmDetails();
        Thread.sleep(6000);
        GetOfferPage getOfferPage=PageFactory.initElements(driver,GetOfferPage.class);
        getOfferPage.clickUploadBnkStmt();
        getOfferPage.updateBankStmt(this.getClass().getClassLoader().getResource("ICICI.pdf").getFile());
        getOfferPage.clickPasswordProtectedCheckBox();
        getOfferPage.setPdfPassword(propFileRead().getProperty("stmtPassword"));
        getOfferPage.clickVerifyNowBtn();
        getOfferPage.clickConfirm();
        SelectPlanPage selectPlanPage=PageFactory.initElements(driver,SelectPlanPage.class);
        Thread.sleep(30000);
        selectPlanPage.clickContinue();
        selectPlanPage.clickSendOtp();
        Assert.assertEquals(propFileRead().getProperty("email"),selectPlanPage.getNameText());
        selectPlanPage.setOtp();
        selectPlanPage.clickVerifyNow();
        KYCPage kycPage=PageFactory.initElements(driver,KYCPage.class);
        kycPage.verifyManually();
        kycPage.clickVerify();
        ExtraInfoPage extraInfoPage= PageFactory.initElements(driver,ExtraInfoPage.class);
        extraInfoPage.clickPhoto();
        extraInfoPage.uploadVideo();
        extraInfoPage.clickPanDiv();
        extraInfoPage.setPanCard(this.getClass().getClassLoader().getResource("PAN-Card.jpg").getFile());
        extraInfoPage.clickUploadPan();
        extraInfoPage.setAddressDetails();
        extraInfoPage.setAccountDetails();
        extraInfoPage.uploadCheque();
        extraInfoPage.location();
        extraInfoPage.setAdditionalDetails();
        extraInfoPage.clickContinueBtn();
        extraInfoPage.clickSubmitApp();
    }




    //todo: Delete application for the below and update the test case
    @Test
    public void selfEmployedItrSubmitted() throws Exception {
        LoginPage loginPage = PageFactory.initElements(driver, LoginPage.class);
        loginPage.prepareForTest(propFileRead().getProperty("mobile_number"));
        loginPage.loginApplication(propFileRead().getProperty("mobile_number"));
        Thread.sleep(5000);
        Assert.assertTrue(loginPage.checkOTPWindow());
        loginPage.setOTP();
        loginPage.clickVerifyNow();
        CheckEligibilityPage checkEligibilityPage = PageFactory.initElements(driver, CheckEligibilityPage.class);
        checkEligibilityPage.selectProfessionalDetails(propFileRead().getProperty("professionalTypeSelfEmp"));
        checkEligibilityPage.selectItrSubmitted("Yes");
        checkEligibilityPage.setMonthlyTakeHome("50000");
        checkEligibilityPage.setEmail(propFileRead().getProperty("email"));
        checkEligibilityPage.isSelfEmpNetBankingEnabled("YES");
        Thread.sleep(2000);
        checkEligibilityPage.clickSelectBankSelfEmpICICIBank();
        Thread.sleep(2000);
        checkEligibilityPage.clickSelectPurposeOfLoan();
        checkEligibilityPage.clickContinueSE();
        checkEligibilityPage.selectGender();
        checkEligibilityPage.setFirstName(propFileRead().getProperty("firstName"));
        checkEligibilityPage.setLastName(propFileRead().getProperty("lastName"));
        checkEligibilityPage.setDob(propFileRead().getProperty("dateOfBirth"));
        checkEligibilityPage.setAreaPinCode(propFileRead().getProperty("areaPinCode"));
        checkEligibilityPage.setPan(propFileRead().getProperty("panNumber"));
        checkEligibilityPage.clickAuthorization();
        checkEligibilityPage.clickContinuePersonalDetailsBtn();
        checkEligibilityPage.clickConfirmDetails();
        Thread.sleep(6000);
        GetOfferPage getOfferPage=PageFactory.initElements(driver,GetOfferPage.class);
        getOfferPage.clickUploadBnkStmt();
        getOfferPage.updateBankStmt(this.getClass().getClassLoader().getResource("ICICI.pdf").getFile());
        getOfferPage.clickPasswordProtectedCheckBox();
        getOfferPage.setPdfPassword(propFileRead().getProperty("stmtPassword"));
        getOfferPage.clickVerifyNowBtn();
        getOfferPage.clickConfirm();
        SelectPlanPage selectPlanPage=PageFactory.initElements(driver,SelectPlanPage.class);
        Thread.sleep(30000);
        selectPlanPage.clickContinue();
        selectPlanPage.clickSendOtp();
        Assert.assertEquals(propFileRead().getProperty("email"),selectPlanPage.getNameText());
        selectPlanPage.setOtp();
        selectPlanPage.clickVerifyNow();
        KYCPage kycPage=PageFactory.initElements(driver,KYCPage.class);
        kycPage.verifyManually();
        kycPage.clickVerify();
        ExtraInfoPage extraInfoPage= PageFactory.initElements(driver,ExtraInfoPage.class);
        extraInfoPage.clickPhoto();
        extraInfoPage.uploadVideo();
        extraInfoPage.clickPanDiv();
        extraInfoPage.setPanCard(this.getClass().getClassLoader().getResource("PAN-Card.jpg").getFile());
        extraInfoPage.clickUploadPan();
        extraInfoPage.setAddressDetails();
        extraInfoPage.setAccountDetails();
        extraInfoPage.uploadCheque();
        extraInfoPage.location();
        extraInfoPage.setAdditionalDetails();
        extraInfoPage.clickContinueBtn();
        extraInfoPage.clickSubmitApp();
    }


    @Test
    public void selfEmployedItrSubmittedIncomeLessThan20000() throws Exception {
        LoginPage loginPage = PageFactory.initElements(driver, LoginPage.class);
        loginPage.prepareForTest(propFileRead().getProperty("mobile_number"));
        loginPage.loginApplication(propFileRead().getProperty("mobile_number"));
        Thread.sleep(5000);
        Assert.assertTrue(loginPage.checkOTPWindow());
        loginPage.setOTP();
        loginPage.clickVerifyNow();
        CheckEligibilityPage checkEligibilityPage = PageFactory.initElements(driver, CheckEligibilityPage.class);
        checkEligibilityPage.selectProfessionalDetails(propFileRead().getProperty("professionalTypeSelfEmp"));
        checkEligibilityPage.selectItrSubmitted("Yes");
        checkEligibilityPage.setMonthlyTakeHome("10500");
        checkEligibilityPage.setEmail(propFileRead().getProperty("email"));
        checkEligibilityPage.isSelfEmpNetBankingEnabled("YES");
        Thread.sleep(2000);
        checkEligibilityPage.clickSelectBankSelfEmpICICIBank();
        Thread.sleep(2000);
        checkEligibilityPage.clickSelectPurposeOfLoan();
        checkEligibilityPage.clickContinueSE();
        Assert.assertTrue(checkEligibilityPage.invalidIncomeWindow());
    }


    @Test
    public void selfEmployedItrNotSubmitted() throws Exception {
        LoginPage loginPage = PageFactory.initElements(driver, LoginPage.class);
        loginPage.prepareForTest(propFileRead().getProperty("mobile_number"));
        loginPage.loginApplication(propFileRead().getProperty("mobile_number"));
        Thread.sleep(5000);
        Assert.assertTrue(loginPage.checkOTPWindow());
        loginPage.setOTP();
        loginPage.clickVerifyNow();
        CheckEligibilityPage checkEligibilityPage = PageFactory.initElements(driver, CheckEligibilityPage.class);
        checkEligibilityPage.selectProfessionalDetails(propFileRead().getProperty("professionalTypeSelfEmp"));
        checkEligibilityPage.selectItrSubmitted("No");
        checkEligibilityPage.setMonthlyTakeHome("10500");
        checkEligibilityPage.setEmail(propFileRead().getProperty("email"));
        checkEligibilityPage.clickContinueSE();
        Assert.assertTrue(checkEligibilityPage.itrNotSubmittedError());
    }

  // @AfterClass
    public void tearDown() {
        driver.quit();
    }
}


