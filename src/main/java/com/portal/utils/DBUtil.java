package com.portal.utils;



import com.portal.reporting.TestLog;

import java.lang.reflect.Field;
import java.sql.*;

public class DBUtil {

    private static Connection connection;

    public DBUtil(String DB_URL, String DB_User, String DB_Password) {
        try {
            Class.forName("com.mysql.jdbc.Driver");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        try {
            connection = DriverManager.getConnection(DB_URL, DB_User, DB_Password);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static ResultSet selectQueryResult(String query) {
        ResultSet rs = null;
        try {
            try {
                Statement stmt = connection.createStatement();
//                TestLog.stepInfo("Firing select query :" + query);
                rs = stmt.executeQuery(query);//               TestLog.stepInfo("Select query has been successfully executed");
            } catch (Exception e) {
                TestLog.stepInfo("Problem in select query, exception " + e.getMessage());

                System.err.println("Got an exception! ");
                System.err.println(e.getMessage());
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return rs;
    }

    public static ResultSet updateQuery(String query) {
        ResultSet rs = null;
        try {
            try {
//                TestLog.stepInfo("Firing update query :" + query);
                Statement stmt = connection.createStatement();
                stmt.executeUpdate(query);
//                TestLog.stepInfo("Update query has been done successfully executed");
            } catch (Exception e) {
                TestLog.stepInfo("Problem in select query, exception " + e.getMessage());
                System.err.println("Got an exception! ");
                System.err.println(e.getMessage());
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return rs;
    }


    public static void deteleQuery(String query){
            try {
                try {
//                TestLog.stepInfo("Firing update query :" + query);
                    Statement stmt = connection.createStatement();
                    stmt.executeUpdate(query);
//                TestLog.stepInfo("Update query has been done successfully executed");
                } catch (Exception e) {
                    TestLog.stepInfo("Problem in delete query, exception " + e.getMessage());
                    System.err.println("Got an exception! ");
                    System.err.println(e.getMessage());
                }

            } catch (Exception e) {
                e.printStackTrace();
            }

        }
}
