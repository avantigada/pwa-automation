

package com.portal.pageFactory;

import com.portal.Queries.QueryConstants;
import com.portal.framework.CommonMethods;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.WebDriverWait;
import java.io.IOException;
import java.lang.reflect.Field;
import java.sql.ResultSet;
import java.util.List;

public class LoginPage extends CommonMethods {
    WebDriverWait wait = new WebDriverWait(driver, 20);

    @FindBy(xpath = ".//input")
    WebElement mobileNbrInput;

    @FindBy(xpath = ".//button")
    WebElement sendOtpBtn;

    @FindBy(xpath = ".//h6[contains(text(),'Mobile')]")
    WebElement otpWindow;

    @FindBy(xpath = ".//input[@type='tel']")
    List<WebElement> optInput;

    @FindBy(xpath = ".//span[contains(text(),'VERIFY NOW')]")
    WebElement verifyNowBtn;

    @FindBy(xpath = ".//h6[contains(text(),'Email ID Verification')]/../following-sibling::div[2]/button")
    WebElement sendOtpForEmail;

    public void deleteOtpLogTable(String mobileNumber){
        String query= QueryConstants.DELETE_OTP.replace("PLACE_HOLDER",mobileNumber).trim();
        dbUtil.deteleQuery(query);
    }

    public void loginApplication(String mobileNumber) throws IOException {
        loadApplicationUrl(propFileRead().getProperty("portalUrl"));
        setTextToElement(mobileNbrInput,mobileNumber);
        explicitWaitAndClick(sendOtpBtn,30);
    }

    public boolean checkOTPWindow() {
        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return otpWindow.isDisplayed();
    }

    public void setOTP() throws Exception{
        String otp = null;
        String query= QueryConstants.GET_OTP.replace("PLACE_HOLDER",propFileRead().getProperty("mobile_number").trim());
        ResultSet resultSet=dbUtil.selectQueryResult(query);
        if(resultSet!=null && resultSet.next()){
           otp= String.valueOf(resultSet.getString("otp"));
        }
        int i=0;
        for (WebElement ele:optInput){
            setTextToElement(ele,String.valueOf(otp.charAt(i++)));
        }
    }

    public void clickVerifyNow(){
        explicitWaitAndClick(verifyNowBtn,30);
    }

    public void prepareForTest(String mobile_number) {
        String deleteUserRole=QueryConstants.DELETE_USERROLE.replace("PLACE_HOLDER",mobile_number).trim();
        String deleteAppUser=QueryConstants.DELETE_APPUSER.replace("PLACE_HOLDER",mobile_number).trim();
        String deleteOtpquery= QueryConstants.DELETE_OTP.replace("PLACE_HOLDER",mobile_number).trim();
        String deleteLoanApplication=QueryConstants.DELETE_LA.replace("PLACE_HOLDER",mobile_number).trim();
        String deleteLoanLead=QueryConstants.DELETE_LL.replace("PLACE_HOLDER",mobile_number).trim();
        dbUtil.deteleQuery(deleteUserRole);
        dbUtil.deteleQuery(deleteAppUser);
        dbUtil.deteleQuery(deleteOtpquery);
        dbUtil.deteleQuery(deleteLoanApplication);
        dbUtil.deteleQuery(deleteLoanLead);
    }
}



