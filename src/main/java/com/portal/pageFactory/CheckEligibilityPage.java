package com.portal.pageFactory;

import com.portal.framework.CommonMethods;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.logging.XMLFormatter;


public class CheckEligibilityPage extends CommonMethods {
    @FindBy(xpath = ".//*[@value='salaried' and @name='employmentType']")
    WebElement salaried;
    @FindBy(xpath = ".//*[@value='self employed - business']")
    WebElement selfEmp;
    @FindBy(xpath = ".//input[@value='cash' and @name='incomeMode']")
    WebElement cash;
    @FindBy(xpath = ".//input[@value='cheque' and @name='incomeMode']")
    WebElement cheque;
    @FindBy(xpath = ".//input[@value='online' and @name='incomeMode']")
    WebElement online;
    @FindBy(xpath = ".//*[@inputmode='numeric' and @type='text']")
    WebElement monthlyTakeHome;
    @FindBy(xpath = ".//*[contains(text(),'Email ID')]/following-sibling::div/input")
    WebElement email;
    @FindBy(xpath = ".//*[@name='netbankingEnabled' and @value=1]")
    WebElement yes;
    @FindBy(xpath = ".//*[@name='netbankingEnabled' and @value=0]")
    WebElement no;
    @FindBy(xpath = ".//*[contains(text(),'Select bank')]/following-sibling::div")
    WebElement selectBankLbl;
    @FindBy(xpath = ".//*[@role='listbox']/li[contains(text(),'HDFC Bank')]")
    WebElement hdfcBnk;
    @FindBy(xpath =".//label[contains(text(),'Purpose of the loan')]")
    WebElement purposeOfLaonLbl;
    @FindBy(xpath = ".//*[@data-value='Events']")
    WebElement purpose;
    @FindBy(xpath = ".//*[contains(text(),'Note: Min Salary ')]/parent::div/following-sibling::div//button")
    WebElement continueBtn;
    @FindBy(xpath = ".//*[contains(text(),'Note: Min income ')]/parent::div/following-sibling::div//button")
    WebElement continueBtnSe;
    @FindBy(xpath = ".//*[contains(text(),'Invalid Take Home Salary')]")
    WebElement salaryEligibilityError;

    //personal details
    @FindBy(xpath = ".//*[@name='gender' and @value='male']")
    WebElement male;

    @FindBy(xpath = ".//label[contains(text(),'First name')]/../div/input")
    WebElement firstName;

    @FindBy(xpath = ".//label[contains(text(),'Last name')]/../div/input")
    WebElement lastName;


    @FindBy(xpath = ".//label[contains(text(),'Date of birth')]/../div/input")
    WebElement dob;

    @FindBy(xpath = ".//label[contains(text(),'Area PIN code')]/../div/input")
    WebElement areaPinCode;

    @FindBy(xpath = ".//label[contains(text(),'PAN number')]/../div/input")
    WebElement pan;

    @FindBy(xpath = ".//input[@type='checkbox']")
    WebElement authorization;

    @FindBy(xpath = ".//span[contains(text(),'I authorize')]/parent::label/parent::div/following-sibling::div//button")
    WebElement continuePersonalDetailsBtn;

    @FindBy(xpath = ".//p[contains(text(),'Your Age ')]")
    WebElement ageErrorWindowText;

    @FindBy(xpath = ".//p[contains(text(),'Area Not Serviceable')]")
    WebElement getAreaPinCodeErrorWindowText;

    @FindBy(xpath = ".//p[contains(text(),'At the moment, we are unable to provide loans to individuals who get salary in cash')]")
    WebElement cashErrorWindow;

    @FindBy(xpath = ".//*[@name='submittedItr' and @value=\"1\"]")
    WebElement itrYes;

    @FindBy(xpath = ".//*[@name='submittedItr' and @value=\"0\"]")
    WebElement itrNo;

    @FindBy(xpath = ".//*[@name='selfNetBanking' and @value=\"1\"]")
    WebElement selfEmpNetBankYes;

    @FindBy(xpath = ".//*[@name='selfNetBanking' and @value=\"0\"]")
    WebElement selfEmpNetBankNo;

    @FindBy(xpath = ".//p[contains(text(),'Invalid Take Home Salary')]")
    WebElement invalidIncomeErrorWindow;

    @FindBy(xpath = ".//p[contains(text(),'Income Tax Return Not Filed')]")
    WebElement itrErrorWindow;








    public void selectProfessionalDetails(String empType){
        if(empType.equalsIgnoreCase("salaried")){
            jsClick(salaried);
        }else{
            jsClick(selfEmp);
        }

    }

    public void selectIncomeMode(String modeOfPayment){
        switch (modeOfPayment){
            case("cash"):
                jsClick(cash);
                break;
            case("cheque"):
                jsClick(cheque);
                break;
            default:
                jsClick(online);


        }

    }

    public void setMonthlyTakeHome(String monthlySalary){
        setTextToElement(monthlyTakeHome,monthlySalary);
    }

    public void setEmail(String emailId){
        setTextToElement(email,emailId);
    }

    public void isNetBankingEnabled(String option){
        if(option.equalsIgnoreCase("YES")){
            jsClick(yes);
        }else{
            jsClick(no);
        }
    }


    public void clickSelectBankICICIBank(){
       driver.findElement(By.xpath(".//*[contains(text(),'Select bank')]/following-sibling::div")).click();
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        driver.findElement(By.xpath(".//*[@role='listbox']/li[contains(text(),'ICICI Bank')]")).click();
    }

    public void clickSelectPurposeOfLoan(){
        driver.findElement(By.xpath(".//*[contains(text(),'Purpose of the loan')]//following-sibling::div")).click();
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        driver.findElement(By.xpath(".//*[@role='listbox']/li[contains(text(),'Events')]")).click();
    }


    public void clickContinueButton(){
        jsClick(continueBtn);

    }


    public boolean getSalaryEligibilityError(){
        return salaryEligibilityError.isDisplayed();
    }

    public void selectGender(){
        jsClick(male);
    }

    public void setFirstName(String  fname){
        setTextToElement(firstName,fname);
    }

    public void setLastName(String  lname){
        setTextToElement(lastName,lname);
    }

    public void setDob(String  dateOfBirth){
        setTextToElement(dob,dateOfBirth);
    }

    public void setAreaPinCode(String  areaPin){
        setTextToElement(areaPinCode,areaPin);
    }

    public void setPan(String  panNo){
        setTextToElement(pan,panNo);
    }


    public void clickAuthorization() {
        jsClick(authorization);
    }

    public void clickContinuePersonalDetailsBtn() {
        jsClick(continuePersonalDetailsBtn);
    }

    public void clickConfirmDetails() {
        driver.findElement(By.xpath(".//p[contains(text(),'Confirm Your Details')]/parent::div/following-sibling::div[3]//button")).click();
    }

    public boolean checkAgeSoftRejectMessage() {
        return ageErrorWindowText.isDisplayed();
    }

    public boolean checkServiceableAreaPinCodeSoftRejectMessage() {
        return getAreaPinCodeErrorWindowText.isDisplayed();
    }

    public boolean cashSoftRejectPopUp() {
        return cashErrorWindow.isDisplayed();

    }

    public void selectItrSubmitted(String itrSubmitted) {
        if(itrSubmitted.equalsIgnoreCase("YES")){
            jsClick(itrYes);
        }else{
            jsClick(itrNo);
        }
    }


    public void isSelfEmpNetBankingEnabled(String option){
        if(option.equalsIgnoreCase("YES")){
            jsClick(selfEmpNetBankYes);
        }else{
            jsClick(selfEmpNetBankNo);
        }
    }


    public void clickSelectBankSelfEmpICICIBank(){
        driver.findElement(By.xpath(".//*[contains(text(),'Select primary bank')]/following-sibling::div")).click();
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        driver.findElement(By.xpath(".//*[@role='listbox']/li[contains(text(),'ICICI Bank')]")).click();
    }

    public void clickContinueSE(){
        jsClick(continueBtnSe);
    }


    public boolean invalidIncomeWindow() {

       return invalidIncomeErrorWindow.isDisplayed();
    }

    public boolean itrNotSubmittedError() {
        return itrErrorWindow.isDisplayed();
    }
}
