package com.portal.pageFactory;

import com.portal.framework.CommonMethods;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class GetOfferPage extends CommonMethods {


    @FindBy(xpath = ".//a")
    WebElement uploadBnkStmt;

    @FindBy(id="bankStatements")
    WebElement bankStmtInput;

    @FindBy(xpath = ".//input[@type='checkbox']")
    WebElement passwordProtectedCheckBox;

    @FindBy(xpath = ".//span[contains(text(),'Verify Now')]/parent::button")
    WebElement verifyNowBtn;

    @FindBy(xpath = ".//input[@type='password']")
    WebElement pdfPassword;

    @FindBy(xpath = ".//p[contains(text(),'Please confirm')]/parent::div/following-sibling::div[2]//button")
    WebElement confirm;


    public void clickUploadBnkStmt(){
        jsClick(uploadBnkStmt);
    }



    public void updateBankStmt(String fileLoc){
        bankStmtInput.sendKeys(fileLoc);
    }

    public void clickPasswordProtectedCheckBox(){
        jsClick(passwordProtectedCheckBox);
    }

    public void clickVerifyNowBtn(){
        jsClick(verifyNowBtn);
    }

    public void setPdfPassword(String password){
        setTextToElement(pdfPassword,password);
    }

    public void clickConfirm() {
        jsClick(confirm);
    }
}
