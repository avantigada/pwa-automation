package com.portal.pageFactory;

import com.portal.framework.CommonMethods;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class KYCPage extends CommonMethods {

    @FindBy(xpath = ".//span[contains(text(),'Verify Instantly')]/parent::button")
    WebElement verifyNow;

    @FindBy(xpath = ".//span[contains(text(),'Verify KYC Manually')]")
    WebElement verifyManual;

    @FindBy(xpath = ".//span[contains(text(),'Continue')]/parent::button")
    WebElement verifyKyc;



    public void clickVerifyNow(){
        jsClick(verifyNow);
    }

    public void verifyManually(){
        jsClick(verifyManual);
    }

    public void clickVerify(){
        jsClick(verifyKyc);
    }
}
