package com.portal.pageFactory;

import com.portal.Queries.QueryConstants;
import com.portal.framework.CommonMethods;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.sql.ResultSet;
import java.util.List;

public class SelectPlanPage extends CommonMethods {

    @FindBy(xpath = ".//span[contains(text(),'CONTINUE')]/parent::button")
    WebElement continueBtn;

    @FindBy(xpath = ".//span[contains(text(),'SEND OTP')]/parent::button")
    WebElement sendOtp;

    @FindBy(xpath = ".//h6[contains(text(),'Email ID')]/following-sibling::h6")
    WebElement emailId;

    @FindBy(xpath = ".//input[@type='tel']")
    List<WebElement> optInput;

    @FindBy(xpath = ".//span[contains(text(),'VERIFY NOW')]/parent::button")
    WebElement verifyNowBtn;



    public void clickContinue(){
        jsClick(continueBtn);
    }

    public void clickSendOtp(){
        jsClick(sendOtp);
    }

    public String getNameText(){
       return emailId.getText();
    }

    public void setOtp() throws Exception{
            Thread.sleep(5000);
            String otp = null;
            String query= QueryConstants.GET_OTP_EMAIL.replace("PLACE_HOLDER",propFileRead().getProperty("email").trim());
            ResultSet resultSet=dbUtil.selectQueryResult(query);
            if(resultSet!=null && resultSet.next()){
                otp= String.valueOf(resultSet.getString("otp"));
            }
            int i=0;
            for (WebElement ele:optInput){
                setTextToElement(ele,String.valueOf(otp.charAt(i++)));
            }
    }

    public void clickVerifyNow(){
        jsClick(verifyNowBtn);
    }



}
