package com.portal.pageFactory;

import com.portal.framework.CommonMethods;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class ExtraInfoPage extends CommonMethods {

    @FindBy(xpath = ".//p[contains(text(),'PAN Card')]/parent::div")
    WebElement panCardDiv;

    @FindBy(id="pancard")
    WebElement panCardInput;

    @FindBy(xpath = ".//*[@id='pancard']/parent::div/parent::div/parent::div/parent::div/parent::div/parent::div/parent::div//button")
    WebElement uploadPan;

    @FindBy(xpath = ".//p[contains(text(),'Permanent Address Proof')]/parent::div")
    WebElement permanentAddressDiv;

    @FindBy(id="permanent_address.0")
    WebElement permanent_address0;

    @FindBy(id="permanent_address.1")
    WebElement permanent_address1;

    @FindBy(xpath = ".//*[@id='permanent_address.1']/parent::div/parent::div/parent::div/parent::div/parent::div/parent::div/parent::div//button")
    WebElement permanentAddressUpload;

    @FindBy(xpath = ".//p[contains(text(),'Select Document')]/parent::div")
    WebElement docTypeForAddress;

    @FindBy(xpath = ".//*[@role='listbox']//*[contains(text(),'Passport')]/parent::li")
    WebElement selectPassport;

    @FindBy(xpath = ".//p[contains(text(),'Salary A/C Details')]/parent::div")
    WebElement salaryDiv;

    @FindBy(xpath = ".//p[contains(text(),'This account')]/parent::div/parent::div/following-sibling::div/div[2]//input")
    WebElement ifscCode;

    @FindBy(xpath =".//label[starts-with(text(),'Account Number')]/following-sibling::div/input" )
    WebElement accNo;
    @FindBy(xpath =".//p[contains(text(),'This account')]/parent::div/parent::div/following-sibling::div/div[4]//input" )
    WebElement confirmAccNo;

    @FindBy(xpath = ".//span[contains(text(),'VERIFY NOW')]/parent::button")
    WebElement verifyNow;

    @FindBy(id= "cancelled_cheque")
    WebElement cancelledCheque;

    @FindBy(xpath = ".//*[@id='cancelled_cheque']/parent::div/parent::div/parent::div/parent::div/parent::div/parent::div/parent::div//button")
    WebElement cancelledChequeUpload;

    @FindBy(xpath = ".//p[contains(text(),'Photo')]/parent::div")
    WebElement photoDiv;

    @FindBy(id = "user-photo-capture")
    WebElement photoImg;

    @FindBy(xpath = ".//span[contains(text(),'CONTINUE')]/following-sibling::span/parent::button")
    WebElement continuePhoto;

    @FindBy(xpath = ".//span[contains(text(),'TAKE PICTURE')]/parent::button")
    WebElement takePhoto;

    @FindBy(xpath = ".//span[contains(text(),'USE THIS PICTURE')]/parent::button")
    WebElement usePhoto;

    @FindBy(id = "user-video-recording")
    WebElement videoImg;

    @FindBy(xpath = ".//span[contains(text(),'TAKE VIDEO')]/parent::button")
    WebElement takeVideo;

    @FindBy(xpath = ".//span[contains(text(),'USE THIS VIDEO')]/parent::button")
    WebElement usevideo;

    @FindBy(xpath = ".//p[contains(text(),'Uploaded photo')]/parent::div/parent::div/parent::div/following-sibling::div//button")
    WebElement uploadPhotoVideo;

    @FindBy(xpath = ".//p[contains(text(),'Your Location')]/parent::div")
    WebElement yourlocDiv;

    @FindBy(xpath = ".//span[contains(text(),'LOCATE ME')]/parent::button")
    WebElement locateMeBtn;

    @FindBy(xpath = ".//p[contains(text(),'Additional Details')]/parent::div")
    WebElement additionalInfoDiv;

    @FindBy(xpath = ".//label[starts-with(text(),'Education Level')]/parent::div")
    WebElement educationLevelDiv;

    @FindBy(xpath = ".//*[@data-value='Graduate']")
    WebElement educationLevel;

    @FindBy(xpath = ".//*[contains(text(),'Mother')]/following-sibling::div/input")
    WebElement motherName;

    @FindBy(xpath = ".//*[contains(text(),'Father')]/following-sibling::div/input")
    WebElement fatherName;

    @FindBy(xpath = ".//*[contains(text(),'Company')]/following-sibling::div/input")
    WebElement companyName;

    @FindBy(xpath = ".//input[@inputmode='email']")
    WebElement workEmail;

    @FindBy(xpath = ".//*[contains(text(),'Office address')]/following-sibling::div/textarea[1]")
    WebElement officeAddress;

    @FindBy(xpath = ".//*[contains(text(),'PIN Code')]/following-sibling::div/input")
    WebElement officePin;

    @FindBy(xpath = ".//span[starts-with(text(),'SAVE')]/parent::button")
    WebElement saveBtn;

    @FindBy(xpath = ".//span[contains(text(),'CONTINUE')]/parent::button")
    WebElement continueBtn;

    @FindBy(xpath = ".//span[contains(text(),'BACK')]/parent::button/parent::div/following-sibling::div//button")
    WebElement submitApplication;


    public void clickPanDiv(){
        jsClick(panCardDiv);
    }

    public void setPanCard(String location){
        setTextToElement(panCardInput,location);
    }

    public void clickUploadPan(){
        jsClick(uploadPan);
    }


    public void clickAddressDiv() throws Exception{
        jsClick(permanentAddressDiv);
        Thread.sleep(2000);
        driver.findElement(By.xpath(".//p[contains(text(),'Select Document')]/parent::div")).click();
        Thread.sleep(2000);
        jsClick(selectPassport);
    }

    public void setAddress1(String location){
        setTextToElement(permanent_address0,location);
    }

    public void setAddress2(String location){
        setTextToElement(permanent_address1,location);
    }

    public void clickUploadAddress(){
        jsClick(permanentAddressUpload);
    }

    public void setIfscCode(String code){
        setTextToElement(ifscCode,code);
    }

    public void setAccNumber(String accNumber) throws Exception{
        jsClick(accNo);
        Thread.sleep(1000);
        setTextToElement(accNo,accNumber);
    }

    public void setConfirmAccNumber(String accNumber){
        jsClick(confirmAccNo);
        setTextToElement(confirmAccNo,accNumber);
    }

    public void clickVerify(){
        jsClick(verifyNow);
    }

    public void clickSalaryDiv(){
        jsClick(salaryDiv);
    }


    public void setAccountDetails() throws Exception{
        clickSalaryDiv();
        setIfscCode(propFileRead().getProperty("ifsc"));
        Thread.sleep(1000);
        setAccNumber(propFileRead().getProperty("bankAccount"));
        setConfirmAccNumber(propFileRead().getProperty("bankAccount"));
        clickVerify();
    }

    public void setAddressDetails() throws Exception{
      clickAddressDiv();
      setAddress1(this.getClass().getClassLoader().getResource("PAN-Card.jpg").getFile());
      setAddress2(this.getClass().getClassLoader().getResource("PAN-Card.jpg").getFile());
      clickUploadAddress();
    }


    public void uploadCheque(){
        clickSalaryDiv();
        setTextToElement(cancelledCheque,this.getClass().getClassLoader().getResource("PAN-Card.jpg").getFile());
        jsClick(cancelledChequeUpload);

    }


    public void clickPhoto() throws  Exception{
        jsClick(photoDiv);
        jsClick(photoImg);
        jsClick(continuePhoto);
        driver.findElement(By.xpath("//span[contains(text(),'TAKE PICTURE')]/parent::button")).click();
        Thread.sleep(1000);
        jsClick(usePhoto);
        Thread.sleep(5000);
    }


    public void uploadVideo() {
        jsClick(videoImg);
        jsClick(continuePhoto);
        jsClick(takeVideo);
        explicitWaitElemPresent(usevideo,5);
        jsClick(usevideo);
        jsClick(uploadPhotoVideo);
    }

    public void location() {
     jsClick(yourlocDiv);
     jsClick(locateMeBtn);
    }

    public void setAdditionalDetails() throws Exception{
        Thread.sleep(2000);
        driver.findElement(By.xpath(".//p[contains(text(),'Additional Details')]/parent::div")).click();;
        Thread.sleep(1000);
        driver.findElement(By.xpath(".//label[starts-with(text(),'Education Level')]/parent::div")).click();
        Thread.sleep(1000);
        driver.findElement(By.xpath(".//*[@data-value='Graduate']")).click();
        setTextToElement(fatherName,"TestFather");
        setTextToElement(motherName,"TestMother");
        setTextToElement(companyName,"TestCompany");
        setTextToElement(workEmail,"testAutomation@gmail.com");
        setTextToElement(officeAddress,"officeAddress officeAddress officeAddress officeAddress");
        setTextToElement(officePin,"560043");
        jsClick(saveBtn);


    }

    public void clickContinueBtn() throws Exception{
        Thread.sleep(3000);
        driver.findElement(By.xpath(".//span[contains(text(),'CONTINUE')]/parent::button")).click();    }


    public void clickSubmitApp() throws Exception{
        Thread.sleep(2000);
        jsClick(submitApplication);
    }
}

