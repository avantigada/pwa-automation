package com.portal.Queries;

public class QueryConstants {
    public static final String DELETE_APPUSER="delete from app_user where phone_number in ( \"PLACE_HOLDER\",\"+91PLACE_HOLDER\");";
    public static final String DELETE_USERROLE="delete from user_role where user_id in (select id from app_user where phone_number IN ( \"PLACE_HOLDER\",\"+91PLACE_HOLDER\"));";
    public static final String DELETE_LA="delete from loan_application where kyc_contact_mobile IN ( \"PLACE_HOLDER\",\"+91PLACE_HOLDER\");";
    public static final String DELETE_LL="delete from loan_application_lead where phone_number IN ( \"PLACE_HOLDER\",\"+91PLACE_HOLDER\");";
    public static final String DELETE_OTP="delete from otp_log where mobile_no IN ( \"PLACE_HOLDER\",\"+91PLACE_HOLDER\");";
    public static final String GET_OTP="select otp from otp_log where mobile_no=\"PLACE_HOLDER\" order by date_modified desc limit 1;";
    public static final String GET_OTP_EMAIL="select otp from  whizdb_loans_qa.otp_log where email =\"PLACE_HOLDER\" order by date_created desc limit 1;";
}
