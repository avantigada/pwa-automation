package com.portal.reporting;

import com.relevantcodes.extentreports.LogStatus;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.testng.Reporter;

import java.awt.*;

public class TestLog {
    static Logger logger = Logger.getLogger("");
    static {
        PropertyConfigurator.configure("config/log4j.properties");
        logger.debug("Log4j console appender configuration is successful !!");
    }

    public static void stepInfo(String message) {
        logger.info(message);
        Reporter.log(message);
        ExtentTestManager.getTest().log(LogStatus.INFO, message);
    }

    // Reporter.log("<font background-color='green' font-weight='bold'
    // >"+message+"</font>");
    // ExtentTestManager.getTest().log(LogStatus.INFO, message);

    public static void stepInfoInGreenColor(String message) {
        String foncolor = Color.GREEN.toString();
        logger.info(message);
        Reporter.log("<font  size='+2' color='green'>" + message + "</font>");
        // Reporter.log("<font background-color='green' font-weight='bold'
        // >"+message+"</font>");
        ExtentTestManager.getTest().log(LogStatus.INFO, message);
    }

    public static void testFail(String message) {
        logger.error(message);
        ExtentTestManager.getTest().log(LogStatus.FAIL, message);
    }

    public static void testPass(String message) {
        logger.info(message);
        ExtentTestManager.getTest().log(LogStatus.PASS, message);
    }

    public static void testStart(String message, String desc) {
        logger.info(message);
        ExtentTestManager.startTest(message, desc);
    }
}
