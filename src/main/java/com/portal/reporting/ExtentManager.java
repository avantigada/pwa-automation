package com.portal.reporting;

import com.relevantcodes.extentreports.ExtentReports;

public class ExtentManager {
    private static ExtentReports extent;
    public synchronized static ExtentReports getReporter(){
        if(extent == null){
            extent = new ExtentReports("testReport.html", true);
        }
        return extent;
    }
}
