package com.portal.framework;

import com.portal.utils.DBUtil;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.DesiredCapabilities;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

public class TestBase {

    public static DBUtil dbUtil=null;
    public static Properties prop;
    public static WebDriver driver = null;



    public static Properties propFileRead() throws IOException {
        prop = new Properties();
        FileInputStream cn = new FileInputStream(Constants.CONFIGFILE_USER_DIR);
        prop.load(cn);
        return prop;
    }

    public  void initialization() throws Exception {

        if (propFileRead().getProperty("browser").equalsIgnoreCase("chrome")) {
            System.setProperty("webdriver.chrome.driver", this.getClass().getClassLoader().getResource("chromedriver").getFile());
            ChromeOptions options = new ChromeOptions();
            options.addArguments("start-maximized");
            options.addArguments("test-type");
            options.addArguments("enable-strict-powerful-feature-restrictions");
            options.addArguments("disable-geolocation");
            options.addArguments("--use-fake-ui-for-media-stream=1");
            options.addArguments("--use-fake-device-for-media-stream");
            DesiredCapabilities cap = new DesiredCapabilities();
            cap.setCapability(ChromeOptions.CAPABILITY, options);
            cap = cap.merge(DesiredCapabilities.chrome());
            driver = new ChromeDriver(cap);
        } else if (propFileRead().getProperty("browser").equalsIgnoreCase("firefox")) {
            driver = new FirefoxDriver();
        }

        driver.manage().window().maximize();
        driver.manage().deleteAllCookies();
        driver.manage().timeouts().implicitlyWait(Constants.IMPLICIT_WAIT, TimeUnit.SECONDS);
    }
}
