package com.portal.framework;

import org.openqa.selenium.*;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import java.lang.reflect.Field;

import java.util.List;

public class CommonMethods extends TestBase {
    //WebDriver driver;
    public void setTextToElement(WebElement ele, String text) {
        ele.sendKeys(text);
    }

    public void maximiseAppScreen() {
        driver.manage().window().fullscreen();
    }

    public void explicitWaitAndClick(WebElement ele, long timeDuration) {
        WebDriverWait webDriverWait = new WebDriverWait(driver, timeDuration);
        webDriverWait.until(ExpectedConditions.visibilityOf(ele));
        ele.click();
    }

    public void explicitWaitElemPresent(WebElement ele, long timeDuration) {
        WebDriverWait webDriverWait = new WebDriverWait(driver, timeDuration);
        webDriverWait.until(ExpectedConditions.visibilityOf(ele));

    }

    public void loadApplicationUrl(String url) {
        driver.get(url);
    }


    public void cropImage(WebElement webElement){

        Actions axn =new Actions(driver);
        axn.moveToElement(webElement,0,0);
        axn.clickAndHold().moveByOffset(257,292).release().build().perform();
    }


    public void uploadDocument(WebElement webElement, String filepPath){
        webElement.sendKeys(filepPath);

    }

    public void jsClick(WebElement element){
        JavascriptExecutor javascriptExecutor=(JavascriptExecutor)driver;
        javascriptExecutor.executeScript("arguments[0].click();",element);
    }


    public String getQueryFromField(Field field, String placeholder) {
        field.setAccessible(true);
        try {
            return field.get(null).toString().replace("PLACE_HOLDER", placeholder).trim();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
        return null;
    }


}
