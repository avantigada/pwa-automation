package com.portal.framework;

import java.io.File;

public class Constants {

    public static final String USER_DIR = System.getProperty("user.dir");
    public static final String USER_HOME = System.getProperty("user.home");
    public static final String CONFIG_DIR = USER_DIR + File.separator + "config" + File.separator;
    public static final String CONFIGFILE_USER_DIR = CONFIG_DIR + "config.properties";
    public static final String RESOURCES_DIR = USER_DIR + File.separator + "resources" + File.separator;
    public static final String CHROMEDRIVER__USER_DIR = RESOURCES_DIR + "chromedriver";
    public static final String FOLDER_DIR = USER_DIR + File.separator + "Desktop" + File.separator;
    public static final String VIDEO_DIR = FOLDER_DIR + File.separator + "Video.mp4";
    public final String BANK_STMT="HDFC.pdf";
    //UploadVideo
    public static final String VIDEO_UPLOAD ="UploadVideo.mp4";
    public static long IMPLICIT_WAIT = 20;
}
